/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sheridan;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Admin
 */
public class CartTest {
    
    public CartTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    @Test
    public void testAddProduct() {
        System.out.println("AddProduct regular");
        String product = "abvvvvij";
        boolean expResult = true;
        boolean result = Cart.AddProduct(product);
        assertEquals("The add product doesnot meet the requirements",expResult, result);

    }
    
    @Test
    public void testAddProductException() {
        System.out.println("AddProduct regular");
        String product = "";
        boolean expResult = false;
        boolean result = Cart.AddProduct(product);
        assertEquals("The add product doesnot meet the requirements",expResult, result);

    }
    
}
