/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sheridan;

/**
 *
 * @author Admin
 */
public class DiscountFactory {
    
    private static DiscountFactory disFactory;
    class DiscountType {
    Percentage unitper;
    Dollar unitdollar;
    
    public enum Percentage {
        PER
    }

    public enum Dollar {
        DOL
    }

    public DiscountType(Percentage unitper, Dollar unitdollar) {
        this.unitper = unitper;
        this.unitdollar = unitdollar;
    }
    }
    
    DiscountFactory(){}
    
    public static DiscountFactory getInstance(){
        if (disFactory == null) {
            disFactory = new DiscountFactory();
        }
        return disFactory;
        }
    public DiscountType getDiscount( DiscountType type, Percentage unitper, Dollar unitdollar){
        DiscountType dis = null;
        switch(type) {
            case PER :
                dis = new Per( unitper );
                break;
            case DOL :
                dis = new Dol( unitdollar );
                break;
        }
        return dis;
    }
}
